// agconnect 配置

const context = {
    agcgw: {
      backurl: "connect-drcn.hispace.hicloud.com",
      url: "connect-drcn.dbankcloud.cn",
      websocketbackurl: "connect-ws-drcn.hispace.dbankcloud.com",
      websocketurl: "connect-ws-drcn.hispace.dbankcloud.cn",
    },
    agcgw_all: {
      CN: "connect-drcn.dbankcloud.cn",
      CN_back: "connect-drcn.hispace.hicloud.com",
      DE: "connect-dre.dbankcloud.cn",
      DE_back: "connect-dre.hispace.hicloud.com",
      RU: "connect-drru.hispace.dbankcloud.ru",
      RU_back: "connect-drru.hispace.dbankcloud.ru",
      SG: "connect-dra.dbankcloud.cn",
      SG_back: "connect-dra.hispace.hicloud.com",
    },
    client: {
      cp_id: "890086000300047629",
      product_id: "99536292102227929",
      client_id: "879832835186246720",
      client_secret:
        "5E9F59FDF2339E49B80BEDD11593D2E1530B7851243BF039D45AEAF4337EDA5E",
      project_id: "99536292102227929",
      app_id: "243650030995739642",
      api_key:
        "DAEDAOay8H5EcFjD/Y7hzAlSHy39K+yE8wPE/MvXZOqpCwtALR0xPPb76Z9gb7nvGbxl0lW5Kj14tzHPoDtW8fbDdpiJV6ux37sYng==",
    },
    oauth_client: {
      client_id: "106083333",
      client_type: 7,
    },
    app_info: {
      app_id: "243650030995739642",
    },
    service: {
      analytics: {
        collector_url:
          "datacollector-drcn.dt.hicloud.com,datacollector-drcn.dt.dbankcloud.cn",
        collector_url_ru:
          "datacollector-drru.dt.dbankcloud.ru,datacollector-drru.dt.hicloud.com",
        collector_url_sg:
          "datacollector-dra.dt.hicloud.com,datacollector-dra.dt.dbankcloud.cn",
        collector_url_de:
          "datacollector-dre.dt.hicloud.com,datacollector-dre.dt.dbankcloud.cn",
        collector_url_cn:
          "datacollector-drcn.dt.hicloud.com,datacollector-drcn.dt.dbankcloud.cn",
        resource_id: "p1",
        channel_id: "",
      },
      search: {
        url: "https://search-drcn.cloud.huawei.com",
      },
      cloudstorage: {
        storage_url: "https://agc-storage-drcn.platform.dbankcloud.cn",
      },
      ml: {
        mlservice_url:
          "ml-api-drcn.ai.dbankcloud.com,ml-api-drcn.ai.dbankcloud.cn",
      },
    },
    region: "CN",
    configuration_version: "3.0",
  };


export {
    context
}
