/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.study.family.cdb;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.study.family.model.FamilyEditFields;

import com.study.family.R;

import java.util.Calendar;

public class EditActivity extends AppCompatActivity {
    static final String ACTION_ADD = "com.huawei.agc.clouddb.quickstart.ADD";

    static final String ACTION_SEARCH = "com.huawei.agc.clouddb.quickstart.SEARCH";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        initViews();
    }

    private void initViews() {
        Intent intent = getIntent();
        String action = intent.getAction();
        EditText bookNameEdit = findViewById(R.id.edit_familyname);

        Button addButton = findViewById(R.id.add);
        Button searchButton = findViewById(R.id.search);
        if (ACTION_ADD.equals(action)) {
            FamilyEditFields.EditMode editMode = FamilyEditFields.EditMode.valueOf(
                intent.getStringExtra(FamilyEditFields.EDIT_MODE));
            View fieldAuthor = findViewById(R.id.field_author);
            fieldAuthor.setVisibility(View.VISIBLE);
            EditText authorEdit = findViewById(R.id.edit_sex);

            View fieldPublisher = findViewById(R.id.field_publisher);
            fieldPublisher.setVisibility(View.VISIBLE);
            EditText publisherEdit = findViewById(R.id.edit_deathday);

            View fieldPublishTime = findViewById(R.id.field_publish_time);
            fieldPublishTime.setVisibility(View.VISIBLE);
            EditText publishTimeEdit = findViewById(R.id.edit_instruction);

            View fieldPriceEdit = findViewById(R.id.field_price);
            fieldPriceEdit.setVisibility(View.VISIBLE);
            EditText priceEdit = findViewById(R.id.edit_birthday);
            Calendar calendar = Calendar.getInstance();
            if (editMode == FamilyEditFields.EditMode.MODIFY) {
                setTitle(R.string.edit_book);
                bookNameEdit.setText(intent.getStringExtra(FamilyEditFields.FAMILY_NAME));
                authorEdit.setText(intent.getStringExtra(FamilyEditFields.SEX));
                priceEdit.setText(intent.getStringExtra(FamilyEditFields.BIRTHDAY));
                publisherEdit.setText(intent.getStringExtra(FamilyEditFields.DEATHDAY));
                publishTimeEdit.setText(intent.getStringExtra(FamilyEditFields.INSTRUCTION));
                addButton.setText(R.string.modify);
            }

            final int bookId = intent.getIntExtra(FamilyEditFields.FAMILY_ID, -1);
            addButton.setOnClickListener(v -> {
                if ("".equals(bookNameEdit.getText().toString()) && "".equals(authorEdit.getText().toString())
                    && "".equals(publisherEdit.getText().toString())) {
                    onBackPressed();
                    return;
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra(FamilyEditFields.FAMILY_ID, bookId);
                resultIntent.putExtra(FamilyEditFields.FAMILY_NAME, bookNameEdit.getText().toString());

                resultIntent.putExtra(FamilyEditFields.BIRTHDAY,priceEdit.getText().toString());

                resultIntent.putExtra(FamilyEditFields.SEX, authorEdit.getText().toString());
                resultIntent.putExtra(FamilyEditFields.DEATHDAY, publisherEdit.getText().toString());
                resultIntent.putExtra(FamilyEditFields.INSTRUCTION, publishTimeEdit.getText().toString());
                setResult(RESULT_OK, resultIntent);
                finish();
            });
            searchButton.setVisibility(View.GONE);
        } else if (ACTION_SEARCH.equals(action)) {
            setTitle(R.string.search_book);
            View fieldShowCount = findViewById(R.id.field_show_count);
            fieldShowCount.setVisibility(View.VISIBLE);
            EditText showCountEdit = findViewById(R.id.edit_show_count);

            View fieldSearchPriceView = findViewById(R.id.field_search_price);
            fieldSearchPriceView.setVisibility(View.VISIBLE);
            EditText lowestPriceEdit = findViewById(R.id.life_birthday);
            EditText highestPriceEdit = findViewById(R.id.life_deathday);
            searchButton.setOnClickListener(v -> {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(FamilyEditFields.FAMILY_NAME, bookNameEdit.getText().toString());
                if (!"".equals(lowestPriceEdit.getText().toString())) {
                    resultIntent.putExtra(FamilyEditFields.BIRTHDAY,lowestPriceEdit.getText().toString());
                }
                if (!"".equals(highestPriceEdit.getText().toString())) {
                    resultIntent.putExtra(FamilyEditFields.DEATHDAY,highestPriceEdit.getText().toString());
                }
                String showCount = showCountEdit.getText().toString();
                if (!showCount.isEmpty()) {
                    resultIntent.putExtra(FamilyEditFields.SHOW_COUNT, Integer.parseInt(showCount));
                }
                setResult(RESULT_OK, resultIntent);
                finish();
            });
            addButton.setVisibility(View.GONE);
        } else {
            // Something wrong, just return
            finish();
            return;
        }

        Button cancelButton = findViewById(R.id.cancel);
        cancelButton.setOnClickListener(v -> {
            setResult(RESULT_CANCELED);
            finish();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
