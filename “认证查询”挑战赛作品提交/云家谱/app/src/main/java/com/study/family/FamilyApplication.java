/*
 * Copyright 2020. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
package com.study.family;

import android.app.Application;
import android.content.Context;

import com.huawei.agconnect.AGCRoutePolicy;
import com.huawei.agconnect.AGConnectInstance;
import com.huawei.agconnect.AGConnectOptions;
import com.huawei.agconnect.AGConnectOptionsBuilder;
import com.huawei.agconnect.auth.AGConnectAuth;
import com.huawei.agconnect.cloud.database.AGConnectCloudDB;
import com.huawei.hms.api.HuaweiMobileServicesUtil;
import com.study.family.model.CloudDBZoneWrapper;

public class FamilyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

    }

    /**
     * set default route policy
     * suggest to call in application#oncreate()
     *
     * @param context applicationContext
     */
    private void setStorageLocation(Context context) {
        AGConnectOptionsBuilder builder = new AGConnectOptionsBuilder()
            .setRoutePolicy(AGCRoutePolicy.CHINA);
        AGConnectInstance.initialize(context, builder);

        AGConnectAuth auth = AGConnectAuth.getInstance();
    }

    /**
     * set route policy
     *
     * @param context applicationContext
     */
    private void setDbLocation(Context context) {

        AGConnectOptions agConnectOptions = new AGConnectOptionsBuilder()
            .setRoutePolicy(AGCRoutePolicy.CHINA)
            .build(context);
        AGConnectInstance instance = AGConnectInstance.buildInstance(agConnectOptions);


        AGConnectAuth auth = AGConnectAuth.getInstance(instance);
        AGConnectCloudDB mCloudDB = AGConnectCloudDB.getInstance(instance, auth);

    }

}
