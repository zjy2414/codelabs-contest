package com.atomlab.daycheck.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.atomlab.daycheck.R;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> implements View.OnClickListener {
   private List<Student> mStudentList;

   private OnItemClickListener mOnItemClickListener;

   public interface OnItemClickListener {
      void onItemClick(View view, int position);
   }

   public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
      this.mOnItemClickListener = onItemClickListener;
   }

   @Override
   public void onClick(View v) {
      if (mOnItemClickListener != null) {
         mOnItemClickListener.onItemClick(v, (int) v.getTag());
      }
   }

   static class ViewHolder extends RecyclerView.ViewHolder {
      View studentView;
      ImageView studentImage;
      TextView studentName;
      public ViewHolder(View view) {
         super(view);
         studentView = view;
         studentImage = view.findViewById(R.id.student_image);
         studentName = view.findViewById(R.id.student_name);
      }
   }
   @NonNull
   @Override
   public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
              .inflate(R.layout.student_item, parent, false);
      final ViewHolder holder = new ViewHolder(view);
      view.setOnClickListener(this);
      return holder;
   }
   @Override
   public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
      holder.itemView.setTag(position);
      Student student = mStudentList.get(position);
      holder.studentName.setText(student.getName());
      holder.studentImage.setVisibility(student.getGreen() == "是"? View.VISIBLE:View.GONE);
   }
   @Override
   public int getItemCount() {
      return mStudentList.size();
   }
   public StudentAdapter(List<Student> studentList) {
      mStudentList = studentList;
   }

}
