# Codelabs挑战赛作品提交仓库
![输入图片说明](%E2%80%9C%E8%AE%A4%E8%AF%81%E6%9F%A5%E8%AF%A2%E2%80%9D%E6%8C%91%E6%88%98%E8%B5%9B%E4%BD%9C%E5%93%81%E6%8F%90%E4%BA%A4/banner.png)

#### 作品提交要求
1.   **将Demo源码提交至此Gitee仓库** 
* 以你的“作品名称”命名文件夹，将demo源码放入文件夹内，并提交Pull Request至对应的作品提交目录，如“认证查询”挑战赛作品提交目录下。
* Pull Request请求合入后则表示文件提交成功，具体方法请参考[如何在Gitee提交Pull Request](https://www.bookstack.cn/read/gitee-opensource-guide-1.0-zh/%E7%AC%AC%E4%B8%89%E9%83%A8%E5%88%86%EF%BC%9A%E5%B0%9D%E8%AF%95%E5%8F%82%E4%B8%8E%E5%BC%80%E6%BA%90-%E7%AC%AC%207%20%E5%B0%8F%E8%8A%82%EF%BC%9A%E6%8F%90%E4%BA%A4%E7%AC%AC%E4%B8%80%E4%B8%AA%20Pull%20Request.md)。

2.   **在华为开发者论坛-Serverless板块以图文方式投稿** 
* 在[【论坛- Serverless板块】](https://developer.huawei.com/consumer/cn/forum/block/serverless)以图文+gif演示形式投稿，全面展示作品功能，发帖标题前带 **【Codelabs挑战赛—认证查询】** 。
* 发帖要求：内容原创，语句通顺，排版整洁。禁止带有色情、政治、外链、推广等内容。
* 华为开发者联盟论坛首发，著作权归发帖人所有，华为有免费使用权。
* 发帖内容符合法律法规要求，不得侵犯第三方合法权益，否则责任自负。

完成以上2步作品提交，方可成功参与活动

#### 了解更多比赛信息
了解最新活动信息，寻求技术帮助请添加官方支持群微信，并备注挑战赛。
![输入图片说明](%E2%80%9C%E8%AE%A4%E8%AF%81%E6%9F%A5%E8%AF%A2%E2%80%9D%E6%8C%91%E6%88%98%E8%B5%9B%E4%BD%9C%E5%93%81%E6%8F%90%E4%BA%A4/vx.png)